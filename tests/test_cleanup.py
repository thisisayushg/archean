from archean.cleanup import (
    MonetaryCleanup,
    TimeCleanup,
    normalize_date,
    normalize_str,
    str_to_list,
)

time_cleanup = TimeCleanup()


def test_to_numeric():
    assert time_cleanup.to_numeric("2h 30min") == 150
    assert time_cleanup.to_numeric("2hours") == 120
    assert time_cleanup.to_numeric("0hours") == 0
    assert time_cleanup.to_numeric("30 minutes") == 30
    assert time_cleanup.to_numeric("1 hour 30 minutes") == 90
    assert time_cleanup.to_numeric("15 hours 10minutes") == 910
    assert time_cleanup.to_numeric("20 minutes 30 seconds") == 20
    assert time_cleanup.to_numeric("20 minutes 30s") == 20
    assert time_cleanup.to_numeric("20 minutes 30sec") == 20

    assert time_cleanup.to_numeric("1hour 30min") == 90
    assert time_cleanup.to_numeric("3h 30sec") == 180
    assert time_cleanup.to_numeric("3h") == 180
    assert time_cleanup.to_numeric("30secs") == 0

    assert time_cleanup.to_numeric("1h 2minutes 5s") == 62
    assert time_cleanup.to_numeric("1h 30 minutes") == 90
    assert time_cleanup.to_numeric("0h 3m 1s") == 3
    assert time_cleanup.to_numeric("8hrs") == 480

    assert time_cleanup.to_numeric("") == 0
    assert time_cleanup.to_numeric("xyz hour") == 0

    assert time_cleanup.to_numeric("1 hour 30 minutes", "sec") == 5400
    assert time_cleanup.to_numeric("15 hours 10minutes", "sec") == 54600
    assert time_cleanup.to_numeric("20 minutes 30 seconds", "sec") == 1230
    assert time_cleanup.to_numeric("20 minutes 30s", "sec") == 1230
    assert time_cleanup.to_numeric("20 minutes 30sec", "sec") == 1230
    assert time_cleanup.to_numeric("8hrs", "sec") == 28800
    assert time_cleanup.to_numeric("1hour 30min", "sec") == 5400
    assert time_cleanup.to_numeric("3h 30sec", "sec") == 10830


monetary_cleanup = MonetaryCleanup()


def test_large_num_names():
    assert monetary_cleanup.large_num_names("$1.25 million") == "$1250000.0"
    assert monetary_cleanup.large_num_names("0 crores") == "0.0"
    assert monetary_cleanup.large_num_names("0.13 billions") == "130000000.0"
    assert monetary_cleanup.large_num_names("Rs 42000") == "Rs 42000"
    assert monetary_cleanup.large_num_names("INR 43,000") == "INR 43,000"
    assert monetary_cleanup.large_num_names("$3 crore") == "$30000000.0"
    assert monetary_cleanup.large_num_names("16 lakh") == "1600000.0"
    assert monetary_cleanup.large_num_names("59 lakhs") == "5900000.0"
    assert (
        monetary_cleanup.large_num_names("\u20B915.5 lakhs") == "\u20B91550000.0"
    )  # \u20B9 is rupee sign
    assert (
        monetary_cleanup.large_num_names("\u20B915.5 lakhs") == "\u20B91550000.0"
    )  # \u20B9 is rupee sign
    assert monetary_cleanup.large_num_names("random sentence") == "random sentence"
    assert (
        monetary_cleanup.large_num_names("$1 million (US rentals)")
        == "$1000000.0 (US rentals)"
    )
    assert (
        monetary_cleanup.large_num_names("$1.8 million (est. US/ Canada rentals)")
        == "$1800000.0 (est. US/ Canada rentals)"
    )
    assert (
        monetary_cleanup.large_num_names("\u20a4427.544 million") == "\u20a4427544000.0"
    )
    assert monetary_cleanup.large_num_names("\u00a3214,452") == "\u00a3214,452"
    # assert (
    #    monetary_cleanup.large_num_names(
    #        "5 crores(equivalent to 84.02 crores or 11 million in 2019)"
    #    )
    #    == "50000000.0(equivalent to 84.02 crores or 11 million in 2019)"
    # )


def test_extract_estimated_amount():
    assert monetary_cleanup.extract_estimated_amount("est. 927498732") == "927498732"
    assert (
        monetary_cleanup.extract_estimated_amount("est. INR29727.97") == "INR29727.97"
    )
    assert (
        monetary_cleanup.extract_estimated_amount("estimated HKD232  (Hong Kong)")
        == "HKD232"
    )
    assert (
        monetary_cleanup.extract_estimated_amount("estimated HKD232  (Hong Kong)")
        == "HKD232"
    )
    assert (
        monetary_cleanup.extract_estimated_amount("~$23989 million") != "$23989 million"
    )
    assert (
        monetary_cleanup.extract_estimated_amount(" ~7889798.767 (US rentals) ")
        == "7889798.767"
    )
    assert (
        monetary_cleanup.extract_estimated_amount("\u20B915.5 lakhs")
        == "\u20B915.5 lakhs"
    )  # no estimated/est/ keyword in the given value
    assert monetary_cleanup.extract_estimated_amount([11, 2, 3]) is None


def test_extract_series_collection():
    assert (
        monetary_cleanup.extract_series_collection("$106759044(Total of 2 films)")
        == "$106759044"
    )
    assert (
        monetary_cleanup.extract_series_collection(
            "$2937437 (Total of 2 theatrical films)"
        )
        == "$2937437"
    )
    assert (
        monetary_cleanup.extract_series_collection("$634450 (USA Gross Total)")
        == "$634450"
    )
    assert (
        monetary_cleanup.extract_series_collection("Total (2 films)$686.6 million")
        == "$686.6"
    )
    assert (
        monetary_cleanup.extract_series_collection("Total (1 film):$856.08 million")
        == "$856.08"
    )
    assert (
        monetary_cleanup.extract_series_collection("£160000 (Australia)£250000 (total)")
        == "£160000"
    )
    assert (
        monetary_cleanup.extract_series_collection(
            "$218626 (USA) (sub-total)£2475758 (UK)"
        )
        == "$218626"
    )


def test_remove_equivalent_amt():
    assert (
        monetary_cleanup.remove_equivalent_amt("1 crore (119 crore as of 2011)")
        == "1 crore"
    )
    assert monetary_cleanup.remove_equivalent_amt([]) is None
    assert (
        monetary_cleanup.remove_equivalent_amt("INR3274 (equivalent to  in 2016)")
        == "INR3274"
    )
    assert (
        monetary_cleanup.remove_equivalent_amt("98779 (equivalent to  or  in 2019)")
        == "98779"
    )
    assert (
        monetary_cleanup.remove_equivalent_amt(
            "₹89.2 crore (equivalent to ₹326 crore (US$46 million) in 2016)"
        )
        == "₹89.2 crore"
    )
    assert monetary_cleanup.remove_equivalent_amt(" test string") == " test string"


def test_money():
    assert monetary_cleanup.money(False) is None
    assert (
        monetary_cleanup.money("$389106 (non-USA)  €411480 (Spain) (20 December 2002)")
        == "$389106"
    )
    assert (
        monetary_cleanup.money("$689000000.0-791000000.0") == "$689000000.0-791000000.0"
    )
    assert monetary_cleanup.money("$36122 (1995 US re-release only)") == "$36122"
    assert (
        monetary_cleanup.money(
            "$8234000  Industry  ByronStuart. Film Comment; New York Vol. 14Iss. 2(..."
        )
        == "$8234000"
    )
    assert monetary_cleanup.money("Get me USD 500 from that guy") == "USD 500"
    assert (
        monetary_cleanup.money("Around \u20AC500 - 600 are left") == "\u20AC500 - 600"
    )
    assert (
        monetary_cleanup.money("Around \u20AC500 - \u20AC600 are left")
        == "\u20AC500 - \u20AC600"
    )
    assert (
        monetary_cleanup.money("$500- \u20AC300") == "$500"
    )  # no range match since currencies are different
    assert monetary_cleanup.money("1995 year movie") == "1995 year movie"


def test_str_to_list():
    assert str_to_list("Ab, Cd, EE") == ["Ab", "Cd", "EE"]
    assert str_to_list("Ab    , Cd, EE") == ["Ab", "Cd", "EE"]
    assert str_to_list("189,000,00") == ["189", "000", "00"]
    assert str_to_list("hazelnut,chocolate,butterscotch") == [
        "hazelnut",
        "chocolate",
        "butterscotch",
    ]
    assert str_to_list(89) == 89
    assert str_to_list("No comma") == ["No comma"]
    assert str_to_list([1, 2, 3]) == [1, 2, 3]


def test_normalize_date():
    assert normalize_date(("2020", "1", "2")) == "2020/01/02"
    assert normalize_date(("2020", "01", "01")) == "2020/01/01"


def test_normalize_str():
    assert normalize_str([]) == []
    assert normalize_str("[[Hello World]]") == "Hello World"
    assert normalize_str("< ref > References: < /ref >") == ""
    assert normalize_str("<ref> References: </ref >") == ""
    assert (
        normalize_str("           Some long spaced   string  ")
        == "Some long spaced string"
    )
    assert normalize_str("Kevin, McMiller, Gutenberg, ") == "Kevin, McMiller, Gutenberg"
    assert (
        normalize_str("Kevin<br/> McMiller<br/> Gutenberg, ")
        == "Kevin, McMiller, Gutenberg"
    )
    assert (
        normalize_str("Kevin<BR/> McMiller<BR/> Gutenberg, ")
        == "Kevin, McMiller, Gutenberg"
    )
    assert normalize_str("Kevin< BR /> McMiller") == "Kevin, McMiller"
    assert normalize_str("Kevin< BR / > McMiller") == "Kevin, McMiller"
    assert normalize_str("USD& nbsp;45000;INR300000") == "USD45000;INR300000"
    assert normalize_str("<!-- Reliable source not found -->") == ""
    assert normalize_str("< !-- Reliable source not found -- >") == ""

    assert (
        normalize_str(
            "USD 45,000,000 ( Toronto ) <ref> https://www.example.com/some/page/< /ref>, Canada <!--Collection reliable source not available -->"
        )
        == "USD 45,000,000 (Toronto), Canada"
    )
