## v3.1.0
- Added: Runtime of movie/show will now be a number in minutes
- Fixed: Multiplier for "millions" keyword in monetary value
- Fixed: Warning message for mFactor variable
- Fixed: Gross/Budget values coming as list of numbers
- Fixed: Starcast containing 'BR' keyword

## v3.0.0
- **Changed command line arguments with all new arguments**. Check the README for more details.
- Fixed: Unknown output of "name: xyz, original: xyz20"
- Added: colors to the console output for readability and context
- Added: PEP8 compliance
- Added: Number of items found in each file will be displayed
## v2.0.0
- **Added support for Windows**
- Updated code for PEP-517 compliance
- Fixed bug: Overall progress keeps in second last position as the download progresses
- Fixed bug: Name of file being downloaded isn't shown

## v1.2.0
-  `tqdm` progressbars
-  `--files` option added to specify the files to download. Only applicable with `download` or `download-only` option.
-  `--download-only` CLI option
-  Bug Fixes

## v1.1.0
